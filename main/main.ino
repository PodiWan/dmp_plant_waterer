#include <string.h>
#include <TimerOne.h>

const int TIMER_LIMIT = 8;

typedef struct _time {
  int hour;
  int minute;
  int second;
} Time;

typedef struct _runningTime {
  Time period;
  Time occurance;
} RunningTime;

Time parseDateTime(const char *input) {
  const char *delimiter = ":";
  char *token;
  int datetime_arr[3] = { 0 };
  int token_count = 0;
  while ((token = strsep(&input, delimiter))) {
    datetime_arr[token_count++] = atoi(token);
  }
  Time interruptTime;
  interruptTime.hour = datetime_arr[0];
  interruptTime.minute = datetime_arr[1];
  interruptTime.second = datetime_arr[2];

  return interruptTime;
}

RunningTime getRunningTime(const char* input) {
  RunningTime runningTime;
  
  const char *delimiter = "-";
  char *token = strtok(input, delimiter);
  runningTime.occurance = parseDateTime(token);
  token = strtok(NULL, delimiter);
  runningTime.period = parseDateTime(token);
  
  return runningTime;
}

RunningTime runningTime = getRunningTime("00:00:00-00:00:00");
long cycle = 0;
const int PUMP_PIN = 13;

void setup() {
  Serial1.begin(9600); // Serial 1 interface for Bluetooth module
  Serial.begin(9600);
  Timer1.initialize();
  Timer1.attachInterrupt(command_routine);
  Timer1.stop();
  pinMode(PUMP_PIN, OUTPUT);
}

void loop() {
  if (Serial1.available()) // Read from Bluetooth and send to PC
  {
    runningTime = getRunningTime(Serial1.readString().c_str());
    Timer1.setPeriod(intervalToMicroSeconds(runningTime.occurance));
  }
  delay(1);
}

long intervalToSeconds(Time interruptTime) {
   return interruptTime.second + 60 * interruptTime.minute + 3600 * interruptTime.hour;
}

long intervalToMicroSeconds(Time interruptTime) {
  return intervalToSeconds(interruptTime) * 1000000;
}

bool shouldTrigger(long cycle, Time interruptTime) {
  return intervalToSeconds(interruptTime) <= TIMER_LIMIT * cycle;
}

void command_routine() {
  if (shouldTrigger(++cycle, runningTime.occurance)) {
    cycle = 0;
    
    digitalWrite(PUMP_PIN, HIGH);
    Timer1.attachInterrupt(execution_routine);
    Timer1.setPeriod(intervalToMicroSeconds(runningTime.period));
  } 
}

void execution_routine() {
  if (shouldTrigger(++cycle, runningTime.period)) {
    cycle = 0;

    digitalWrite(PUMP_PIN, LOW);
    Timer1.attachInterrupt(command_routine);
    Timer1.setPeriod(intervalToMicroSeconds(runningTime.occurance));
  }
}
